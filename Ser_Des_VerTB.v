`timescale 1ns / 1ps

module Ser_Des_VerTB;

	integer in_f;
	integer out_f;
	
	// wej�cia
	reg DesIN;
	reg clk;
	reg reset;
	reg [15:0] SerIn;
	
	//po��czenie
	wire connect;
	

	// wyj�cia
	wire [16:0] DesOUT;
	wire SerOut;


	assign connect = SerOut;
	
	// Instantiate the Unit Under Test (UUT)
	DESERIALIZER uut (
		.DesIN(connect), 
		.clk(clk), 
		.reset(reset), 
		.DesOUT(DesOUT)
	);
	
	SERIALIZER utt (
		.SerIn(SerIn),
		.clk(clk),
		.reset(reset),
		.SerOut(SerOut)
		);
		

	initial begin
		// Initialize Inputs
		DesIN = 0;
		clk = 0;
		reset = 0;
		in_f = $fopen("wejscieVerilog.txt", "r");
	end
	
	//zegar
	always begin
		#5 clk =~clk;
	end

	initial begin
		repeat (1) @ (posedge clk);
		reset = 1;
		while (! $feof(in_f)) begin
			@(negedge clk);
			$fscanf (in_f,"%b \n", SerIn[15:0]);
			#160;
		end
		repeat (256) @ (posedge clk);
		$fclose (in_f);
		#1 $finish;
	end
	
		//zapis do pliku
	
	initial begin
		out_f = $fopen("wyjscieVerilog.txt", "w");
		if(!out_f) begin
			$display("nie udalo sie otworzyc pliku!");
			$finish;
		end
		repeat (512) @(posedge clk);
		$fclose(out_f);
		#5120 $finish;
	end
	
		always @(posedge clk) begin
		#2;
		$fwrite(out_f,"reset=%b|clk=%b|wejscie=%b|wyjscie=%b|transmisjia:%b \n", reset, clk, SerIn, DesOUT[15:0],DesOUT[16]);
	end


      
endmodule

