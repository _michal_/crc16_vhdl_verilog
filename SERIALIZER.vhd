library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


entity SERIALIZER is
	PORT (SerIn  : in std_logic_vector (15 downto 0);
			clk 	 : in std_logic;
			reset	 : in std_logic;
			SerOut : out std_logic
			);
end SERIALIZER;

architecture Behavioral of SERIALIZER is
-- sygnaly wewnetrzne
signal rejestrPrzesuwny : std_logic_vector (15 downto 0);
signal licznik : std_logic_vector (3 downto 0); 
signal CRCSer : std_logic_vector (15 downto 0):= "0000000000000000"; 
signal licznikKoniecPaczki : std_logic_vector (7 downto 0); 
signal buforCRCSer : std_logic_vector (15 downto 0) := "0000000000000000"; 

begin

licznikA : process (reset, clk)
begin
if (reset = '0') then
	licznik<= (others => '0');
elsif (clk'Event and clk = '1') then
	if (licznikKoniecPaczki = "10000000" and licznik < "010000"  ) then 
		licznik <= licznik + "01"; 
	elsif (licznik < "1111" and licznikKoniecPaczki < "10000000") then 
		licznik <= licznik + "01";
	else
		licznik <= "0000"; 
	end if;

end if;
end process licznikA;

licznikKoncaPaczki : process (reset, clk)
begin
if (reset = '0') then
	licznikKoniecPaczki <= (others => '0');
elsif (clk'Event and clk ='1') then
	if (licznikKoniecPaczki <= "10001110") then 
		licznikKoniecPaczki <= licznikKoniecPaczki + "01";
	else
		licznikKoniecPaczki <= (others => '0'); 
	end if;
end if;
end process licznikKoncaPaczki;

rejestrPrzesuwania : process (clk, reset)
begin
if (reset = '0') then
	 rejestrPrzesuwny <= (others => '0');
elsif (clk'Event and clk = '1') then
	if (licznik = "0000" and licznikKoniecPaczki <= "10000000") then 
		 rejestrPrzesuwny <= SerIn; 
	elsif (licznik = "0000" and licznikKoniecPaczki = "10010000") then
		 rejestrPrzesuwny <= SerIn;
	elsif (licznikKoniecPaczki < "10000000") then 
		 rejestrPrzesuwny <= '0' & rejestrPrzesuwny (15 downto 1); 
	elsif (licznikKoniecPaczki > "01111110") then 
		 rejestrPrzesuwny <= (others => '0'); 
	end if;
end if;
end process rejestrPrzesuwania;


BuforowanieCRCSer : process (clk, reset)
begin
if (reset = '0') then
	 buforCRCSer <= (others => '0');
elsif (clk'Event and clk = '1') then
	if (licznikKoniecPaczki = "01111111" ) then
		buforCRCSer <= CRCSer; 
	elsif (licznikKoniecPaczki > "01111111") then 
		buforCRCSer <= '0' & buforCRCSer (15 downto 1);
	end if;
end if;
end process BuforowanieCRCSer;

CRC16Serializer : process (clk, reset)
begin
if (reset = '0') then
	 CRCSer <= (others => '0');
elsif (clk'Event and clk = '1') then
	if (licznikKoniecPaczki <= "01111111") then 
			CRCSer(15) <= (rejestrPrzesuwny(0) xor CRCSer(0)) xor  '0';		
			CRCSer(14) <= CRCSer(15);
			CRCSer(13) <= CRCSer(14);
			CRCSer(12) <= CRCSer(13);
			CRCSer(11) <= CRCSer(12);
			CRCSer(10) <= CRCSer(11) xor (CRCSer(0) xor rejestrPrzesuwny(0));
			CRCSer(9) <= CRCSer(10);
			CRCSer(8) <= CRCSer(9);
			CRCSer(6) <= CRCSer(7);
			CRCSer(5)<= CRCSer(6);
			CRCSer(4)<= CRCSer(5);
			CRCSer(3)<= CRCSer(4) xor (CRCSer(0) xor rejestrPrzesuwny(0));
			CRCSer(7) <= CRCSer(8);
			CRCSer(2)<= CRCSer(3);
			CRCSer(1)<= CRCSer(2);
			CRCSer(0)<= CRCSer(1);
	else
		CRCSer <= (others => '0');  
	end if;
end if;
end process CRC16Serializer;


SerOut <= rejestrPrzesuwny(0) when (licznikKoniecPaczki <= "01111111") else 
			 buforCRCSer(0)	   when (licznikKoniecPaczki > "01111111"); 

end Behavioral;

