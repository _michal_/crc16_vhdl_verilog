
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_textio.all;
use std.textio.all;
 

 
ENTITY SER_DES IS		
END SER_DES;
 
ARCHITECTURE behavior OF SER_DES IS 
 
-- komponenty
 
    COMPONENT SERIALIZER
    PORT(	
         SerIn : IN  std_logic_vector (15 downto 0);
         clk : IN  std_logic;
         reset : IN  std_logic;
         SerOut : OUT  std_logic

        );
    END COMPONENT;
	 
	 COMPONENT DESERIALIZER
	 PORT(	
		DesIN : IN std_logic;
          	clk : IN  std_logic;
           	reset : IN  std_logic;
           	DesOUT : OUT std_logic_vector (16 downto 0)
			 );
	 END COMPONENT;
	 
-- sta�e
constant period : time := 10 ns;
constant p10			: time := period/10;
constant edge			: time := period -p10;
	 
--sygnaly do wczytywania i odczytywania z pliku
signal strobe			       : std_logic;

--wyjscia
signal DesOUT               : std_logic_vector(16 downto 0);
signal SerOut		          : std_logic;

--wejscia
signal SerIn                : std_logic_vector(15 downto 0);
signal clk    			       : std_logic;
signal reset  			       : std_logic;
signal DesIN  			       : std_logic;
 
BEGIN

															
blok1: SERIALIZER PORT MAP (SerIn => SerIn, clk => clk, reset => reset, SerOut => SerOut);

DesIN <= SerOut;

blok2: DESERIALIZER PORT MAP ( DesIN => DesIN, clk => clk, reset => reset, DesOUT => DesOUT);


zegar : process
begin
clk <= '0'; wait for period/2;
clk <= '1'; wait for period/2;
end process zegar;

--wczytywanie z pliku
input : process
file infile		: text is in "wejscie.txt";
variable line_in 	: line;
variable bytes   	: std_logic_vector(16 downto 0);

begin
reset <= '0';
SerIn <= "0000000000000000";
wait for  period;
wait until (clk'Event and clk = '0');
wait for p10;	
while not (endfile(infile)) loop
	readline (infile, line_in);
	read (line_in, bytes);
	SerIn <= bytes (15 downto 0);
	reset <= bytes(16);  --16 bit to reset 
	wait for  16 * period;
end loop;
assert false severity failure;
end process input;
	
strobe <= TRANSPORT clk AFTER edge;
output: process (strobe)

variable str			:string(1 to 42);
variable lineout 		:line;
variable init_file	:std_logic := '1';
file outfile			:text is out "wyjscie.txt";

function	conv_to_char (sig: std_logic) return character is
begin
	case sig is
		when '1'					=> return '1';
		when '0'					=> return '0';
		when 'Z'					=> return 'Z';
		when others					=> return 'X';
	end case;
end conv_to_char;

function conv_to_string (inp: std_logic_vector; length: integer) return string is
variable s : string (1 to length);
begin
	for i in 0 to (length-1) loop
	s(length-i) := conv_to_char(inp(i));
end loop;
	return s;
end conv_to_string;

begin 
--informacje o pliku
	if init_file = '1' then
		str:="Michal Pekala 230153                      ";
		write(lineout,str); writeline(outfile,lineout);
		str:="Wynik symulacji w srodowisku testowym VHDL";
		write(lineout,str); writeline(outfile,lineout);
		str:="Semestr zimowy 2018/2019                  ";
		write(lineout,str); writeline(outfile,lineout);
		str:="Plik wyjsciowy                            ";
		write(lineout,str); writeline(outfile,lineout);
		str:="ukladu kontroli                           ";
		write(lineout,str); writeline(outfile,lineout);
		str:="poprawnosci transmisji                    ";
		write(lineout,str); writeline(outfile,lineout);
		str:="danych                                    ";
		write(lineout,str); writeline(outfile,lineout);
		str:="Je�eli transmisja = 1                     ";
		write(lineout,str); writeline(outfile,lineout);
		str:="to transmisja poprawna                    ";
		write(lineout,str); writeline(outfile,lineout);
		str:="                                          ";
		write(lineout,str); writeline(outfile,lineout);
		str:="clk                                       ";
		write(lineout,str); writeline(outfile,lineout);
		str:="| reset                                   ";
		write(lineout,str); writeline(outfile,lineout);
		str:="| | transmisja                            ";
		write(lineout,str); writeline(outfile,lineout);
		str:="| | | DANEwejsciowe                       ";
		write(lineout,str); writeline(outfile,lineout);
		str:="| | | |                 DANEwyjsciowe     ";
		write(lineout,str); writeline(outfile,lineout);
		str:="| | | |                 |                 ";
		write(lineout,str); writeline(outfile,lineout);
		init_file := '0';
		
	end if;
	
	-- podzia� sygna��w
	if (strobe'Event and strobe = '0') then
		str:= (others => ' ');
		str(1) := conv_to_char(clk);
		str(2) := '|';
		str(3) := conv_to_char(reset);
		str(4) := '|';
		str(5) := conv_to_char(DesOUT(16));
		str(6) := '|';
		str(7 to 23) := conv_to_string(SerIn,16);
		str(24) := '|';
		str(25 to 41) := conv_to_string(DesOUT,16);
		str(42) :='|';
		write(lineout,str);
		writeline(outfile,lineout);
	end if;
	end process output;
			
		
		
		
		


		


end;
