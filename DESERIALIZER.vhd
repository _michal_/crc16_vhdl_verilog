library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_uNSIGNED.all;



entity DESERIALIZER is
	PORT (DesIN : in std_logic;
			clk 	 : in std_logic;
			reset	 : in std_logic;
			DesOUT : out std_logic_vector (16 downto 0)
			);
end DESERIALIZER;

architecture Behavioral of DESERIALIZER is
signal licznikPaczki : std_logic_vector (7 downto 0);
signal licznik : std_logic_vector (3 downto 0);
signal bufor : std_logic_vector (15 downto 0);
signal buforCRCDes : std_logic_vector (15 downto 0);
signal CRCDes : std_logic_vector (15 downto 0) := "0000000000000000";



begin


licznikPaczek : process (clk, reset)
begin
if (reset = '0') then
	licznikPaczki <= (others => '0');
elsif (clk'Event and clk = '1') then
	if (licznikPaczki <= "10001110") then
			licznikPaczki <= licznikPaczki + "01";
	else
			licznikPaczki <= (others => '0');
	end if;
end if;
end process licznikPaczek;

licznikDzieleniaNaPaczki16Bit : process (clk, reset)
begin
if (reset = '0') then
	licznik <= (others => '0');
elsif (clk'Event and clk ='1') then
	if (licznikPaczki = "10000000" and licznik < "010000") then
		licznik <= licznik + "01";
	elsif (licznik < "1111"  and licznikPaczki < "10001110") then
		licznik <= licznik + "01";
	else
		licznik <= (others => '0');
	end if;
end if;
end process licznikDzieleniaNaPaczki16Bit;

CRC16Deserializer : process (clk, reset)
begin
if (reset = '0') then
	 CRCDes <= (others => '0');
elsif (clk'Event and clk = '1') then
	if(licznikPaczki <= "01111111") then
			CRCDes(15) <= (DesIN xor CRCDes(0)) xor  '0';		
			CRCDes(14) <= CRCDes(15);
			CRCDes(13) <= CRCDes(14);
			CRCDes(12) <= CRCDes(13);
			CRCDes(11) <= CRCDes(12);
			CRCDes(10) <= CRCDes(11) xor (CRCDes(0) xor DesIN);
			CRCDes(9)  <= CRCDes(10);
			CRCDes(8)  <= CRCDes(9);
			CRCDes(6)  <= CRCDes(7);
			CRCDes(5)  <= CRCDes(6);
			CRCDes(4)  <= CRCDes(5);
			CRCDes(3)  <= CRCDes(4) xor (CRCDes(0) xor DesIN);
			CRCDes(7)  <= CRCDes(8);
			CRCDes(2)  <= CRCDes(3);
			CRCDes(1)  <= CRCDes(2);
			CRCDes(0)  <= CRCDes(1);
	 else
			CRCDes <= (others => '0');
	 end if;
end if;
end process CRC16Deserializer;

Buforowanie : process (clk, reset)
begin
if (reset = '0') then
	bufor <= (others => '0');
elsif (clk'Event and clk = '1') then
			bufor <= DesIN & bufor(15 downto 1);
end if;
end process Buforowanie;

BuforowanieCRCDes : process (clk, reset)
begin
if (reset = '0') then
	buforCRCDes <= (others => '0');
elsif (clk'Event and clk = '1') then
	if (licznikPaczki = "01111111") then
		buforCRCDes <= CRCDes;
	end if;
end if;
end process BuforowanieCRCDes;



 
DesOUT(15 downto 0) <= (others => '0') when (licznikPaczki <= "00001000" and licznik > "0000") else
							  bufor when licznikPaczki = "00000001" else
							bufor when (licznik = "0001" and licznikPaczki < "01111111");
							  
						  
DesOUT(16) <= '1' when (licznikPaczki = "00000000" and bufor = buforCRCDes) else
				  '0';
		
		
end Behavioral;


	



